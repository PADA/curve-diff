from scipy import integrate, interpolate
from numpy import array
import csv
from collections import namedtuple
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from math import radians, cos, sin, asin, sqrt, ceil, floor
import sys, random
import numpy as np

# TEST 
fx = interpolate.interp1d([1,2,3], [2,4,6])
fy = interpolate.interp1d([1,2,3], [1, 2, 3])
f = lambda t: (fx(t), fy(t))
#print f(1.5)
norm = lambda t: (fx(t) ** 2 + fy(t) ** 2) ** 0.5

#print integrate.quad(norm, 1, 3)




def haversine(lat1, lon1, lat2, lon2):
    """
    Calculate the great circle distance between two points 
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians 
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    # haversine formula 
    dlon = lon2 - lon1 
    dlat = lat2 - lat1 
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a)) 
    r = 6371 # Radius of earth in kilometers. Use 3956 for miles
    return c * r

class Point(namedtuple("Point", ["type", "name", "time", "latitude", "longitude", "alt"])):
    @property
    def timestamp(self):
        return (self.time - datetime(1970, 1, 1)).total_seconds()


def parse_time(time):
    # "2014-11-17 09:18:37"
    return datetime.strptime(time, "%Y-%m-%d %H:%M:%S")



def parse(datafile):
    print "Start reading: %s" % datafile

    with open(datafile) as f:
        before = datetime(1970, 1, 1)
        points = []
        reader = csv.reader(f)
        header = f.readline() # ignore column names
        window = [] 
        for row in reader:
            if row == header:
                continue
            curr = parse_time(row[1])
            if curr < before:
          #      print "original curr: %s" % curr
                curr += timedelta(hours=12)
           #     print "adjusted curr: %s" % curr
           #     print "before: %s" % before
                assert before <= curr
            lat = float(row[2])    
            lng = float(row[3])
            p = Point(type=row[0], name=None, time=curr, latitude=lat, longitude=lng, alt=row[4])
            if len(points) == 0:
                points.append(p)
            elif len(window) == 0:
                window.append(p)
            elif (window[0].latitude == lat) and (window[0].longitude == lng):
                window.append(p)
            else:
                median_point = window[len(window)/2]
                points.append(median_point)
                window = [p]
            #print p
            before = curr


        # digest last window
        print len(window)
        points.append(window[len(window)/2])
        return points


def sample(mylist, count):
    return [ mylist[i] for i in sorted(random.sample(xrange(len(mylist)), count)) ]


class Path:

    def __init__(self, points):
        if len(points) < 2:
            raise ValueError("Path needs at least two points, given %d"  % len(points))

        self.points = points
        self.latitude_func = interpolate.interp1d(self.timestamps, self.latitudes)
        self.longitude_func = interpolate.interp1d(self.timestamps, self.longitudes)

    @property
    def timestamps(self):
        return [ (p.time - datetime(1970, 1, 1)).total_seconds() for p in self.points]
    
    @property
    def latitudes(self):
        return [p.latitude for p in self.points]

    @property
    def longitudes(self):
        return [p.longitude for p in self.points]


    @property
    def location(self):
        return lambda t: (self.latitude_func(t), self.longitude_func(t))
    
    def total_length(self):
        pass

    @property
    def start_point(self):
        return self.points[0]

    @property
    def end_point(self):
        return self.points[-1]


def overlapping_interval(path1, path2):
    start = max(path1.start_point, path2.start_point)
    end = min(path1.end_point, path2.end_point)
    if start > end:
        raise ValueError("No overlapping interval exists (end < start)")

    return (start, end)
    

daejo = parse("151129_105814_daejo.txt")
#sampled = sample(daejo, len(daejo) / 512)
p1 = Path(parse("151129_105812_norm.txt"))
p2 = Path(daejo)

print p1.start_point
print p2.start_point

start, end = overlapping_interval(p1, p2)
s, e = int(ceil(start.timestamp)), int(floor(end.timestamp))
print "effective duration: %d" % (e - s)


d =  0.0
step = 1
samples = float(e-s) / float(step)
max_error = 0.0
min_error = sys.float_info.max
ignored = 1

values = []
for t in xrange(s, e, step):
    loc1 = p1.location(t)
    loc2 = p2.location(t)
    #print loc1, loc2
    #print loc1[1], loc2[1]

    s = float(haversine(loc1[0], loc1[1], loc2[0], loc2[1]))

    values.append(s)


    if s > max_error:
        max_error = s
    if s < min_error:
        min_error = s

    d += s 

print d

variance = np.var(values)
stdev = np.std(values)
print "total seconds: %d" % (e-s) 
print "samples: %d" % (e-s)
print "average error(m): %f" % (1000.0 * (float(d) / (samples)))
print "max error(m): %f" % (1000.0 * (max_error))
print "min error(m): %f" % (1000 * min_error)
print "variance: %f" % variance
print "stdev: %f" % stdev
#print p1.timestamps
#print p1.latitudes
print "#p1 data: %d" % len(p1.points)
print "#p2 data: %d" % len(p2.points)

plt.plot(p1.longitudes, p1.latitudes)
plt.plot(p2.longitudes, p2.latitudes)
plt.show()


